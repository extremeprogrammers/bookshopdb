 public Node find(String title)
    {
        if(root == null)
            return null;

        Node current = root;
        while(!current.bookrecord.getTitle().equals(title))
        {
            if(current.bookrecord.getTitle().compareTo(title) < 0)


                current = current.leftChild;
            else
                current = current.rightChild;
            if(current == null)
                return null;
        }
        return current;