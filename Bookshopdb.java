import java.io.*;
import java.util.*;

class bookrecords
{
    private string fname;
    private string surname;
    private int isbn;

    public void setFname(string fname) {
        this.fname = fname;
    }

    public void setSurname(string surname) {
        this.surname = surname;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public string getFname() {
        return fname;
    }

    public string getSurname() {
        return surname;
    }

    public int getIsbn() {
        return isbn;
    }
 
}
    
class Node
{
    public int bookrecord;
    public Node leftChild;
    public Node rightChild;
    public void displayNode()
    {
        System.out.print("[");
        System.out.print(item);
        System.out.print("]");
    }
}

class Tree
{
    private Node root;
    public Tree()
            
    { root = null; }
    public Node find(int val)
    {
        Node current = root;
        while(current.item != val)
        {
            if(val < current.item)
                current = current.leftChild;
            else
                current = current.rightChild;
            if(current == null)
                return null;
        }
        return current;
    } 
    public void insert(int id)
    {
        Node newNode = new Node();
        newNode.item = id;
        if(root==null)
            root = newNode;
        else
        {
            Node current = root;
            Node parent;
            int key;
            while(true)
            {
                parent = current;
                if(id < current.item)
                {
                   key = bookRecord.getTitle();
                    if(key.compareTo(current.key) < 0 )
                    {
                        parent.leftChild = newNode;
                        return;
                    }
                } 
                else
                {
                    current = current.rightChild;
                    if(current == null) 
                    {
                        parent.rightChild = newNode;
                        return;
                    }
                } 
            } 
        } 
    } 
    public boolean delete(int val) 
    {
        Node current = root;
        Node parent = root;
        boolean isLeftChild = true;
        while(current.item != val)
        {
            parent = current;
            if(val < current.item)
            {   
                isLeftChild = true;
                current = current.leftChild;
            }
            else
            {
                isLeftChild = false;
                current = current.rightChild;
            }
            if(current == null)
                return false;
 
        } 
        if(current.leftChild==null && current.rightChild==null)
        {
            if(current == root)
                root = null;
            else if(isLeftChild)
                parent.leftChild = null;
            else
                parent.rightChild = null;
        }
        else if(current.rightChild==null)
        {
            if(current == root)
                root = current.leftChild;
            else if(isLeftChild)
                parent.leftChild = current.leftChild;
            else
                parent.rightChild = current.leftChild;
        }
        else if(current.leftChild==null)
        {
            if(current == root)
                root = current.rightChild;
            else if(isLeftChild)
                parent.leftChild = current.rightChild;
            else
                parent.rightChild = current.rightChild;
        }
        else
        {
            Node successor = findSuccessor(current);
            if(current == root)
                root = successor;
            else if(isLeftChild)
                parent.leftChild = successor;
            else
                parent.rightChild = successor;
            successor.leftChild = current.leftChild;
        } 
        return true;
    } 
    private Node findSuccessor(Node delNode)
    {
        Node successorParent = delNode; 
        Node successor = delNode;
        Node current = delNode.rightChild;
        while(current != null)
        {
            successorParent = successor;    
            successor = current;
            current = current.leftChild;
        }
        if(successor != delNode.rightChild)
        {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
        return successor;
    }
    public void displayTree()
    {
        Stack globalStack = new Stack();
        globalStack.push(root); 
        int emptyLeaf = 32;
        boolean isRowEmpty = false;
        System.out.println("****......................................................****");
        while(isRowEmpty==false)
        {
 
            Stack localStack = new Stack();
            isRowEmpty = true;
            for(int j=0; j<emptyLeaf; j++)
                System.out.print(' ');
            while(globalStack.isEmpty()==false)
            {
                Node temp = globalStack.pop();
                if(temp != null)
                {
                    System.out.print(temp.item);
                    localStack.push(temp.leftChild);
                    localStack.push(temp.rightChild);
                    if(temp.leftChild != null ||temp.rightChild != null)
                        isRowEmpty = false;
                }
                else
                {
                    System.out.print("--");
                    localStack.push(null);
                    localStack.push(null);
                }
                for(int j=0; j<emptyLeaf*2-2; j++)
                    System.out.print(' ');
            }
            System.out.println();
            emptyLeaf /= 2;
            while(localStack.isEmpty()==false)
                globalStack.push( localStack.pop() );
        }
    System.out.println("****......................................................****");
    } 
    
    public static void main(String[] args)
    {
        Tree bst = new Tree();

       BookRecord record = new BookRecord();
       
       BookRecord record1 = new BookRecord();
        
               

        record.setIsbn(12355);
        record.setTitle("Java");
        record.setFname("anushka");
        record.setSurname("Dharmapala");
        bst.insert(record);  
       
       
      //bst.deleteByName("Java");
       
            
        
     bst.print("Java");
      //bst.print("PHP");
     
     
    
     bst.find("Java").toString();       
        
        System.out.println("");
        
        
       
       
       
        
    
       
    }
}

